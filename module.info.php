<?php

return [
    'name'        => 'Undefined Module',
    'author'      => 'Undefined Author',
    'description' => 'Undefined Description',
    'namespace'   => 'Skimia\\Newsletter',
    'require'     => ['skimia.pages']
];