<?php
namespace Skimia\Newsletter\Traits;

trait MailchimpTrait{

    protected $mc;

    public function mailchimp(){

        $this->mc = new \Mailchimp(\Config::get('skimia.newsletter::api.apikey'));
        curl_setopt($this->mc->ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->mc->ch, CURLOPT_SSL_VERIFYPEER, 0);

        return $this->mc;

    }


}