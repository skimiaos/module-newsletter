<?php

namespace Skimia\Newsletter\Controllers;

use Skimia\Newsletter\Traits\MailchimpTrait;

class Newsletter extends \Controller
{

    use MailchimpTrait;

    public function index(){

        $validator = \Validator::make(\Input::only('email'),['email' => 'required|email']);

        if ($validator->fails())
        {
            //dd($validator->messages());
            return $validator->messages();
        }else{
            $email_adress = \Input::get('email');
            //dd($email_address);
            try{
                $this->mailchimp()->lists->subscribe (\Config::get ( 'skimia.newsletter::api.list_id' ) , [
                    'email' => $email_adress
                ], null,'html',false );
            }catch(\Exception $e){


                $this->mailchimp()->lists->unsubscribe(\Config::get ( 'skimia.newsletter::api.list_id' ) , [
                    'email' => $email_adress
                ]);

                return [
                    'message'=> 'votre adresse '. $email_adress .' a été rayée de notre newsletter.'
                ];
            }




            return [
                'message'=> 'votre adresse ' . $email_adress .' a été iniscrite à notre newsletter.'
            ];
        }


        //dd(json_encode(['message'=>'ff']));


    }
}
