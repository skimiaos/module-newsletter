<?php
/**
 * Created by PhpStorm.
 * User: Jean-françois
 * Date: 23/01/2015
 * Time: 15:51
 */

namespace Skimia\Newsletter\Data\Forms;

use Hugofirth\Mailchimp\MailchimpWrapper;
use Skimia\Config\Form\ConfigForm;
use Skimia\Newsletter\Traits\MailchimpTrait;

class ConfigFormMailChimp extends ConfigForm{

    use MailchimpTrait;


    protected $id = 'website.mailchimp';
    protected $name = 'API Mailchimp';
    protected $description = 'Lier votre site a votre newsletter Mailchimp.';
    protected $icon = 'os-icon-paper-plane-3';
    protected $template = 'skimia.config::forms.simple';

    protected $configs = [];

    protected function makeFields(){

        $this->configs = [
            'skimia.newsletter::api.apikey' => ['label'=>'Clef API du compte MailChimp','type'=>'text'],

            'skimia.newsletter::api.list_id'=>['label'=>'Liste par defaut pour les nouvelles inscriptions','type'=>'select',
                'choicesFct'=> function($field){
                    if(\Config::get('skimia.newsletter::api.apikey') == 'your-key-here')
                        return [
                            '0'=>'Veuillez dabord définir une clef Api'
                        ];

                    try{

                        $choices = [];
                        $lists = $this->mailchimp()->lists->getList()['data'];
                        foreach ($lists as $list) {
                            $choices[$list['id']] = $list['name'];
                        }
                        return $choices;
                    }catch(\Exception $e){
                        return [
                            '0'=>'Veuillez utiliser une clef Api valide'
                        ];
                    }



                }
            ],
        ];
        return parent::makeFields();
    }


    public function getOtherValues(){
        return false;

        return [
            'themes'=>[
                \Theme::getThemes()
            ]
        ];
    }
}