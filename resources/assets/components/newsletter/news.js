
$('#newsletter-subscribe-form-loader').hide();
$(document).ready(function() {

    jQuery.fn.extend({
        disable: function(state) {
            return this.each(function() {
                this.disabled = state;
            });
        }
    });

    $('#newsletter-subscribe-form-loader').hide();
    // Lorsque je soumets le formulaire
    $('#newsletter-subscribe-form').on('submit', function(e) {
        e.preventDefault(); // J'empêche le comportement par défaut du navigateur, c-à-d de soumettre le formulaire

        var $this = $(this); // L'objet jQuery du formulaire

        // Je récupère les valeurs
        var mail = $('#newsletter-subscribe-form-email').val();

        // Je vérifie une première fois pour ne pas lancer la requête HTTP
        // si je sais que mon PHP renverra une erreur
        if(mail === '') {
            alert('Le champ doit être rempli');
        } else {
            // Envoi de la requête HTTP en mode asynchrone

            $('#newsletter-subscribe-form-submit').disable(true);
            $('#newsletter-subscribe-form-email').disable(true);
            $('#newsletter-subscribe-form-loader').show();

            $.ajax({
                url: $this.attr('action'), // Le nom du fichier indiqué dans le formulaire
                type: $this.attr('method'), // La méthode indiquée dans le formulaire (get ou post)
                data: {email: mail}, // Je sérialise les données (j'envoie toutes les valeurs présentes dans le formulaire)
                success: function(html) { // Je récupère la réponse du fichier PHP

                    $('#newsletter-subscribe-form-submit').disable(false);
                    $('#newsletter-subscribe-form-email').disable(false);

                    $('#newsletter-subscribe-form-loader').hide();
                    alert(html.message); // J'affiche cette réponse
                }
            });
        }
    });
});