




<form id="newsletter-subscribe-form" method="post" action="{{route('skimia.newsletter::components.mailchimp')}}">
    <label for="newsletter-subscribe-form-email">Email</label>
    <input id="newsletter-subscribe-form-email" name="email" type="email">

    <button id="newsletter-subscribe-form-submit" type="submit">S'inscrire</button>
    <div id="newsletter-subscribe-form-loader">Chargement en cours...</div>
</form>

